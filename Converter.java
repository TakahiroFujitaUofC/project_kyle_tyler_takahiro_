public class Converter {
//Your names go here:
/*
* Kyle deBoer
* Tyler Chan
* Takahiro Fujita 
*
*/
private double celsiusToFahrenheit(double C){
    double F = (C*(9/5))+32;
    return F;
}
private double fahrenheitToCelsius(double F){
    double C;
    C = (F-32)*(5/9);	
    return C;
}
public static void main(String[] args) {
//TODO: The first student will implement this method.
    double celsiusToFahrenheit(180);//Call CelsiusToFahrenheit to convert 180 Celsius to Fahrenheit value.
    double fahrenheitToCelsius(250);// Call FahrenheitToCelsius to convert 250 Fahrenheit to Celsius value.
}
}
